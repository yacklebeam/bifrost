#include "base.h"
#include <stdio.h>

u64 ReadEntireFile(char *FileName, void *Buffer)
{
    FILE *fp;
    long lSize;

    fp = fopen (FileName , "rb");
    if(!fp)
        return -1;

    fseek(fp , 0L , SEEK_END);
    lSize = ftell(fp);
    rewind(fp);

    /* copy the file into the buffer */
    if(1!=fread(Buffer, lSize, 1, fp))
    {
        fclose(fp);
        return -1;
    }

    fclose(fp);

    return (u64)lSize;
}

u64 GetFileSize(char *FileName)
{
    FILE *fp;
    long lSize;

    fp = fopen (FileName , "rb");
    if(!fp)
        return -1;

    fseek(fp , 0L , SEEK_END);
    lSize = ftell(fp);
    rewind(fp);

    fclose(fp);

    return (u64)lSize;
}
