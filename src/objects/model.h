#ifndef BIFROST_MODEL_H
#define BIFROST_MODEL_H

#include "mesh.h"
#include <vector>

namespace bf
{
    class Model
    {
    public:
        Model(float* vertices, unsigned int vertex_array_size, int* indices, unsigned int index_count);
        void Draw();
        void Cleanup();
    private:
        unsigned int vao_;
        unsigned int vbo_;
        unsigned int ebo_;
        unsigned int vertex_count_;
        unsigned int index_count_;
    };   
}

#endif