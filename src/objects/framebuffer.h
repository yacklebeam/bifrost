#ifndef BIFROST_FRAMEBUFFER_H
#define BIFROST_FRAMEBUFFER_H

namespace bf
{
    class FrameBuffer
    {
    public:
        FrameBuffer();

        unsigned int ID() {return id_;};

        void Activate() const;
        void Deactivate() const;

    private:
        unsigned int id_;
    };
}

#endif