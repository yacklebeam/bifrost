#ifndef BIFROST_SHADER_H
#define BIFROST_SHADER_H

#include <string>
#include "../externals/glm/vec4.hpp" // glm::vec4
#include "../externals/glm/mat4x4.hpp" // glm::mat4

namespace bf
{
    class Shader
    {
    public:
        Shader(const char* vert_shader_file, const char* frag_shader_file);

        unsigned int ID() {return id_;};

        void Use() const;
        void SetBool(const char* name, bool value) const;
        void SetFloat(const char* name, float value) const;
        void SetInt(const char* name, int value) const;
        void SetVec4(const char* name, unsigned int count, glm::vec4 value) const;
        void SetMat4(const char* name, unsigned int count, unsigned int transpose, glm::mat4 value) const;

    private:
        unsigned int id_;

        void CheckCompileErrors(unsigned int shader, std::string type);
    };
}

#endif