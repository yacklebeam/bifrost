#version 430 core

layout (location = 0) in vec3 v_Position;
layout (location = 1) in vec2 v_TexturePosition;

uniform mat4x4 u_MVP;

out vec2 v_Texture;

void main()
{
    v_Texture = v_TexturePosition;
    gl_Position = u_MVP * vec4(v_Position, 1.0);
}