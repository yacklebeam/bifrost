#ifndef BIFROST_HEADERS_H_
#define BIFROST_HEADERS_H_

#if _WIN32
#include <windows.h>
#endif

#include <glad/gl.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#ifndef BIFROST_EXCLUDE_IMGUI
#include <imgui.h>
#include <backends/imgui_impl_glfw.h>
#include <backends/imgui_impl_opengl3.h>
#endif

namespace bifrost
{
    struct Context
    {
        GLFWwindow* window;
        double frame_time;
        double time;
    };
}

#endif // BIFROST_HEADERS_H_

